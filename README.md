Kooba Frontend Challenge
===

You've made to the Kooba technical challenge... Nice one!

# Time limit
Please spend no more than 3-4 hours at a maximum on this task. If you don't get it finished within that time it's not a problem.

# Before you start
* Read through the brief below in detail and download the Sketch file below to check out the design.
* Estimate how much time would be needed to complete the challenge. As mentioned above, if you don't think it's achievable within 3-4 hours above, that's fine.
* Make some notes about your planned approach to the task.
* Share your estimation and planned approach with me by email (ronan.mccormack@kooba.ie) before you start coding.

# The brief

Using HTML, CSS (SCSS preferably) and Javascript, we would like you to recreate the following design, including a search takeover screen:

*Design on Invision*:https://projects.invisionapp.com/share/8311FC5VFQMV

*Sketch File & Image Assets*: https://drive.google.com/drive/folders/1jIvh6uLr30DZ0Ny1u4mH7T_sToiLshY4?usp=sharing

# Some notes
* The main banner should fill the available viewport area.
* The search takeover should fill the entire screen when active.
* ES6+ Javascript should be used to handle showing/hiding the search takeover
* We're looking for clean code that follows best practices
* Whatever you build should work properly in the latest versions of Chrome, Firefox and Safari. IE is not important here (Hooray!)
* Creativity - We would love to see your creativity shine: animations, micro-interactions, general inventiveness. Go wild!
* Keep performance and accessibility in mind.
* Please avoid using any styling frameworks (Bootstrap, Tailwind etc.)
* Kooba base project files are included in this repo - we would love to see you use them.

When you're ready to deliver, please set up a repository on github or bitbucket, and share access with ronan.mccormack@kooba.ie

If you have any questions about this assignment, feel free to get in touch with me: ronan.mccormack@kooba.ie

---

---

# Kooba Frontend base project #
You can use our base project by cloning this repo, although if you'd rather not, that's fine too.

## Gulp 4 + Webpack 4 + Babel + BrowserSync ##

For Live reloading, Browsersync has been used. For ES6 Transpilation, Babel has been used. For SourceMaps, Gulp & Webpack both have been used. Sass/CSS Workflow.

## Setup

- Install [Node](https://nodejs.org/)
- Use *Npm* that comes with Node pre-installed
- Install Gulp globally through `npm install -g gulp@next`
- Install Webpack globally through `npm install -g webpack`
- Clone this project
- `cd` to the cloned project
- Update the remote git repository to the relevant project eg. `git remote set-url origin https://koobafe@bitbucket.org/koobafe/climate-focus.git`
- Install all [packages](./package.json) with `npm install`

## Usage

- **Build the Project and Serve locally (for Development)** - `npm run dev` or `yarn run dev`. The Development port is `3000`. Build directory is `/tmp`
- **Build the Project and Serve locally (for Production)** - `npm start` or `yarn start`. The Production port is `8000`. Build directory is `/dist`
- **Build the Project Only (for Production)** - `npm run build` or `yarn run build`. Build directory is `/dist`
- **Exporting the Project to zip file** - `npm run export` or `yarn run export`

Important Note: **Don't** run these npm scripts simultaneously.

## Appendix

- **Tooling** - Gulpfile Lives in `gulpfile.js` and Webpack config files live within `webpack` folder.
- **Source Files** - Lives in `website/src` folder
- **Compiled Files for development** - Generated into `website/tmp` folder.
- **Compiled Files for production and sharing with backend** - Generated into `website/dist` folder.
