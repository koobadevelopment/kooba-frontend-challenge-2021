const entry = require('./entry');
const nodeModulesToTranspile = require('./nodeModulesToTranspile');

module.exports = {
    entry,
    output: {
        filename: '[name].js'
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "vendors",
                    chunks: "all"
                }
            }
        },
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: new RegExp(`node_modules/(?!(${nodeModulesToTranspile.join('|')})/).*`),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            [
                                '@babel/preset-env',
                                {
                                    useBuiltIns: 'usage',
                                    corejs: 3,
                                    modules: false
                                }
                            ]
                        ]
                    }
                }
            }
        ]
    }
};
