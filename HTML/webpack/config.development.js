const sharedConfig = require('./config.shared');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const devConfig = {
    mode: 'development',
    devtool: 'inline-cheap-source-map',
    performance: {
        hints: false
    },
    plugins: [
        new BundleAnalyzerPlugin()
    ]
};

module.exports = Object.assign(sharedConfig, devConfig);
