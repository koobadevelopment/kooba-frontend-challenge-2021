import objectFitImages from 'object-fit-images';
import polyfills from './polyfills';

polyfills();

window.addEventListener('DOMContentLoaded', () => {
    objectFitImages();
});
