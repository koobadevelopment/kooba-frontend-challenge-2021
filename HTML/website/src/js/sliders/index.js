import InlineDotsSlick from './InlineDotsSlick';
import PagingInfoSlick from './PagingInfoSlick';
import StandardSlick from './StandardSlick';
import TimerSlick from './TimerSlick';
import initSliders from './initSliders';


export {
    InlineDotsSlick,
    PagingInfoSlick,
    StandardSlick,
    TimerSlick,
    initSliders
};
