const sharedConfig = require('./config.shared');

const productionConfig = {
    mode: 'production',
    devtool: false
};

module.exports = Object.assign(sharedConfig, productionConfig);
