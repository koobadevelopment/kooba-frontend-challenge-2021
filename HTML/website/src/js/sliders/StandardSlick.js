import * as $ from 'jquery';
import 'slick-carousel';
import defaultSlickSettings from './defaultSlickSettings';

class StandardSlick {
    constructor(containerElement, slickSettings = defaultSlickSettings) {
        this.containerElement = containerElement;
        this.slickSettings = slickSettings;
        this.sliderElement = this.containerElement.classList.contains('slider')
            ? this.containerElement
            : this.containerElement.querySelector('.slider');
    }

    preInitStatic() {
        // Do something maybe
    }

    preInit() {
        // Do something maybe
    }

    postInit() {
        // Do something maybe
    }

    init() {
        this.preInitStatic();
        this.preInit();
        $(this.sliderElement).slick(this.slickSettings);
        this.postInit();
    }
}

export default StandardSlick;
